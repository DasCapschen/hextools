use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "hextools")]
struct ApplicationOptions {
    ///skip this amount of bytes from the beginning
    #[structopt(short, long)]
    skip: Option<usize>,

    ///only process this amount of bytes
    #[structopt(short, long)]
    take: Option<usize>,

    ///read data from this file
    #[structopt(name = "FILE", required = true)]
    file: PathBuf,

    #[structopt(subcommand)]
    cmd: SubCommand,
}
#[derive(Debug, StructOpt)]
enum SubCommand {
    Find(FindOptions),
    Print,
}

#[derive(Debug, StructOpt)]
struct FindOptions {
    ///the exact data to find, NOT regex! only accepts 0-9A-F
    #[structopt(name = "PATTERN", required = true)]
    pattern: String,
}

fn main() {
    let opt = ApplicationOptions::from_args();

    let file = opt.file;
    let skip = opt.skip.unwrap_or(0);
    let take = opt.take.unwrap_or(std::usize::MAX);

    match opt.cmd {
        SubCommand::Find(options) => {
            hexfind(file, skip, take, options.pattern);
        }
        SubCommand::Print => {
            hexprint(file, skip, take);
        }
    }
}

//TODO: is there a better way for this?
fn char_to_byte(c: char) -> u8 {
    match c {
        '0' => 0x00,
        '1' => 0x01,
        '2' => 0x02,
        '3' => 0x03,
        '4' => 0x04,
        '5' => 0x05,
        '6' => 0x06,
        '7' => 0x07,
        '8' => 0x08,
        '9' => 0x09,
        'a' | 'A' => 0x0A,
        'b' | 'B' => 0x0B,
        'c' | 'C' => 0x0C,
        'd' | 'D' => 0x0D,
        'e' | 'E' => 0x0E,
        'f' | 'F' => 0x0F,
        _ => panic!("Not a valid hex character!"),
    }
}

fn hexfind(path: PathBuf, skip: usize, take: usize, pattern: String) {
    //open file and prepare argument values
    let file = File::open(path).unwrap();

    let search: Vec<u8> = pattern
        .chars()
        .collect::<Vec<char>>() //can only chunk() on slice, so...
        .chunks_exact(2)
        .map(|hex| (char_to_byte(hex[0]) << 4) + char_to_byte(hex[1])) //calculate byte from 2 chars
        .collect();

    //create iterator over file bytes
    let mut itr = file.bytes().skip(skip).take(take);

    //TODO: transform to iterator logic
    let mut idx_data = 0;
    let mut idx_search = 0;
    let mut found = false;

    while let Some(Ok(b)) = itr.next() {
        if idx_search == search.len() {
            found = true;
            break;
        }
        idx_search = if b == search[idx_search] {
            idx_search + 1
        } else {
            0
        };

        idx_data += 1;
    }

    if found {
        println!(
            "Pattern of length {} found at byte offset {}",
            search.len(),
            idx_data - idx_search
        );
    } else {
        println!("Pattern not found!");
    }
}

fn hexprint(path: PathBuf, skip: usize, take: usize) {
    //load arguments
    let file = File::open(path).unwrap();

    //create iterator over bytes
    let mut bytes = file.bytes().skip(skip).take(take);

    //TODO: transform to iterator logic
    while let Some(Ok(b)) = bytes.next() {
        //leading zero if necessary, always 2 chars wide, as hex
        print!("{:02x}", b);
    }
    println!();
}
